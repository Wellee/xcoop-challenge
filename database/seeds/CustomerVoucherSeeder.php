<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerVoucherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer_voucher = [
            ['customer_id' => 1, 'voucher_id' => 1],
            ['customer_id' => 1, 'voucher_id' => 2],
            ['customer_id' => 2, 'voucher_id' => 3],
            ['customer_id' => 2, 'voucher_id' => 4],
            ['customer_id' => 1, 'voucher_id' => 4],
        ];

        DB::table('customer_voucher')->insert($customer_voucher);
    }
}
