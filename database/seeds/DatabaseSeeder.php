<?php

use Database\Seeders\CustomerSeeder;
use Database\Seeders\CustomerVoucherSeeder;
use Database\Seeders\VoucherSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CustomerSeeder::class);
        $this->call(VoucherSeeder::class);
        $this->call(CustomerVoucherSeeder::class);
    }
}
