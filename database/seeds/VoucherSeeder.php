<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class VoucherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vouchers = [
            ['description' => 'Descuento 10%', 'ticket' => 123, 'expiration_date' => Carbon::create('2022', '05', '10')],
            ['description' => 'Descuento 20%', 'ticket' => 124, 'expiration_date' => Carbon::create('2022', '01', '30')],
            ['description' => 'Descuento 30%', 'ticket' => 125, 'expiration_date' => Carbon::create('2022', '03', '30')],
            ['description' => 'Descuento 40%', 'ticket' => 126, 'expiration_date' => Carbon::create('2022', '08', '15')],
        ];

        DB::table('vouchers')->insert($vouchers);
    }
}
