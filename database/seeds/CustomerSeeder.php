<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = [
            ['name' => 'Esteban', 'lastname' => 'Lapissonde'],
            ['name' => 'Nombre', 'lastname' => 'Apellido'],
        ];

        DB::table('customers')->insert($customers);
    }
}
