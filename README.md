# Steps to run the project:

1. Create database and configure .env file
2. Run php artisan migrate
3. Run php artisan db:seed
4. Run php artisan serve
