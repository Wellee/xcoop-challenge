<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Database\QueryException;

class CustomerController extends Controller
{
    public function getCustomerVouchers($customer_id)
    {
        try {
            //get the customer with the id and only the specified columns
            $customer = Customer::where('id', $customer_id)
                ->with('vouchers:id,description,expiration_date')
                ->first();

            //if the customer exists, return the vouchers associated with it
            if ($customer != null) {
                return $customer->vouchers->toJson();
            }

            return response()->json([
                'error' => 'El cliente no existe',
            ], 422);
        } catch (QueryException $e) {
            dd($e->getMessage());
        }
    }
}
