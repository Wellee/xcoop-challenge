<?php

namespace App\Http\Controllers;

use App\Voucher;

use Illuminate\Database\QueryException;
use Carbon\Carbon;

class VoucherController extends Controller
{
    public function checkVoucherValidity($ticket)
    {
        try {
            //Get the voucher with the ticket number
            $voucher = Voucher::firstWhere('ticket', $ticket);

            //If the vouchers exists
            if ($voucher != null) {
                $date = new Carbon($voucher->expiration_date);

                return response()->json([
                    //if the date is greater or equal to today return true else return false
                    'validity' => $date->gte(Carbon::Now()->toDateString()),
                ]);
            }

            return response()->json([
                'error' => 'El voucher no existe',
            ], 422);
        } catch (QueryException $e) {
            dd($e->getMessage());
        }
    }
}
