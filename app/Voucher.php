<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $hidden = ['pivot'];

    public function customers()
    {
    	return $this->belongsToMany(Customer::class);
    }
}
