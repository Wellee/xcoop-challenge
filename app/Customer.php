<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    protected $hidden = ['pivot'];

    public function vouchers()
    {
        return $this->belongsToMany(Voucher::class);
    }
}
